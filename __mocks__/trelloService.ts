import trelloService from '../src/global/services/trello';
import boardsResolved from './stubs/trello/get/boardsResolved.json';
import cardResolved from './stubs/trello/get/cardResolved.json';
import cardsResolved from './stubs/trello/get/cardsResolved.json';
import userResolved from './stubs/trello/get/userResolved.json';

export default jest.mock('../src/global/services/trello', () => {
  trelloService.api.get = jest.fn().mockImplementation((url, params = {}) => {
    switch (url) {
      case '/members/me/boards': {
        return { data: boardsResolved };
      }

      case '/cards/5c017570b2eae50c33dd0599': {
        return { data: cardResolved };
      }

      case '/lists/5bd70c16706ddd7f5ed22511/cards': {
        return { data: cardsResolved };
      }

      case '/members/me': {
        return { data: userResolved };
      }

      default:
        break;
    }
  });

  return trelloService;
});
