import * as History from 'history';

export default {
  history: {
    action: '' as History.Action,
    block: jest.fn(),
    createHref: jest.fn(),
    go: jest.fn(),
    goBack: jest.fn(),
    goForward: jest.fn(),
    length: 0,
    listen: jest.fn(),
    location: {} as History.Location,
    push: jest.fn(),
    replace: jest.fn(),
  },
  location: {
    hash: '',
    key: '',
    pathname: '',
    search: '',
    state: {},
  },
  match: {
    params: {},
    isExact: true,
    path: '',
    url: '',
  },
};
