import { get, set, unset } from 'lodash';
import storage from '../src/global/helpers/storage';

export default jest.mock('../src/global/helpers/storage', () => {
  storage.has = jest.fn().mockReturnValue(true);

  storage.set = jest.fn().mockImplementation((key: string, value: any) => {
    const prevValue = get(storage.store, key);

    storage.store = set(storage.store, key, value);

    storage.emit('change', key, value, prevValue);
  });

  storage.unset = jest.fn().mockImplementation((key: string) => {
    const prevValue = get(storage.store, key);

    if (unset(storage.store, key)) {
      storage.emit('change', key, undefined, prevValue);
    }
  });

  return storage;
});
