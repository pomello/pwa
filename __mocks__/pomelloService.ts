import pomelloService from '../src/global/services/pomello';
import settingsResolved from './stubs/pomello/get/settingsResolved.json';
import usersResolved from './stubs/pomello/get/usersResolved.json';
import authorizeRejected from './stubs/pomello/post/authorizeRejected.json';
import authorizeResolved from './stubs/pomello/post/authorizeResolved.json';

export default jest.mock('../src/global/services/pomello', () => {
  pomelloService.api.get = jest.fn().mockImplementation((url, params = {}) => {
    switch (url) {
      case '/settings':
        return { data: settingsResolved };

      case '/users':
        return { data: usersResolved };

      default: {
        return {};
      }
    }
  });

  pomelloService.api.post = jest.fn().mockImplementation((url, params = {}) => {
    switch (url) {
      case '/authorize': {
        if (params.password === 'bad password') {
          throw authorizeRejected;
        }
        return { data: authorizeResolved };
      }

      default:
        return {};
    }
  });

  // Since authGoogleUser hits the non-API url, it doesn't use the service's API.
  pomelloService.authGoogleUser = jest.fn().mockImplementation(() => ({
    success: true,
    data: authorizeResolved,
  }));

  return pomelloService;
});
