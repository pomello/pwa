import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import routeComponentProps from '../../../../__mocks__/routeComponentProps';
import mockStore from '../../../../__mocks__/store';
import { TaskPage, TaskPageProps, TaskPageState } from '../TaskPage';

let wrapper: ShallowWrapper<TaskPageProps, TaskPageState, TaskPage>;

const taskPageProps = {
  ...routeComponentProps,
  match: {
    ...routeComponentProps.match,
    params: {
      idCard: 'B1ZBLgSv7yLmH9u9',
    },
  },
  bootstrapped: true,
  storeListTasks: jest.fn(),
  storeTasks: jest.fn(),
  timerActive: mockStore.timer.active,
};

beforeEach(() => {
  wrapper = shallow(<TaskPage {...taskPageProps} />);
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders Clock view correctly', () => {
  wrapper.setState({ view: 'clock' });

  expect(wrapper).toMatchSnapshot();
});

it('renders Debrief view correctly', () => {
  wrapper.setState({ view: 'debrief' });

  expect(wrapper).toMatchSnapshot();
});

it('renders Finished view correctly', () => {
  wrapper.setState({ view: 'finished' });

  expect(wrapper).toMatchSnapshot();
});

it('calls storeTasks and storeListTasks when task is not defined', async () => {
  await wrapper.instance().fetchTask();

  expect(taskPageProps.storeTasks).toBeCalled();
  expect(taskPageProps.storeListTasks).toBeCalled();
});
