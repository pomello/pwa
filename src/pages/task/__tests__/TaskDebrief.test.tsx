import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import routeComponentProps from '../../../../__mocks__/routeComponentProps';
import mockStore from '../../../../__mocks__/store';
import storage from '../../../global/helpers/storage';
import { getLists } from '../selectors';
import { TaskDebrief, TaskDebriefProps } from '../TaskDebrief';

let wrapper: ShallowWrapper<TaskDebriefProps, null, TaskDebrief>;

const taskDebriefOwnProps = {
  board: mockStore.app.boards.AXRhAnTqW26gb7va,
  bootstrapped: true,
  currentList: mockStore.app.currentList,
  history: routeComponentProps.history,
  location: routeComponentProps.location,
  nextBreak: 'short',
  unsetCurrentTask: jest.fn(),
};

const lists = getLists(mockStore, taskDebriefOwnProps);

beforeEach(() => {
  wrapper = shallow(<TaskDebrief {...taskDebriefOwnProps} lists={lists} />);
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('shows the last used list if exists', () => {
  storage.get = jest.fn().mockReturnValue('AXRhAnTqW26gb7vd');

  const lastUsedListWrapper = shallow(
    <TaskDebrief {...taskDebriefOwnProps} lists={lists} />
  );

  expect(lastUsedListWrapper).toMatchSnapshot();
});
