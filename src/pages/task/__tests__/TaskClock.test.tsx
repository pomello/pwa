import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import mockStore from '../../../../__mocks__/store';
import { TaskClock, TaskClockProps } from '../TaskClock';

let wrapper: ShallowWrapper<TaskClockProps, {}, TaskClock>;

const taskDialProps = {
  addTimerMarker: jest.fn(),
  board: mockStore.app.boards.AXRhAnTqW26gb7va,
  bootstrapped: true,
  createTimer: jest.fn(),
  destroyTimer: jest.fn(),
  endSoundSrc: mockStore.app.settings.taskTimerEndSound,
  endSoundVol: mockStore.app.settings.taskTimerEndVol,
  gracePeriod: null,
  handleTaskFinish: jest.fn(),
  idList: mockStore.app.lists.AXRhAnTqW26gb7vb.shortId,
  routeToBreak: jest.fn(),
  setCurrentList: jest.fn(),
  setCurrentTask: jest.fn(),
  setTimerMarker: jest.fn(),
  startGracePeriod: jest.fn(),
  startSoundSrc: mockStore.app.settings.taskTimerStartSound,
  startSoundVol: mockStore.app.settings.taskTimerStartVol,
  task: mockStore.app.tasks.B1ZBLgSv7yLmH9u9,
  taskStartTime: mockStore.app.settings.taskTime,
  tickSoundSrc: mockStore.app.settings.taskTimerTickSound,
  tickSoundVol: mockStore.app.settings.taskTimerTickVol,
  timerActive: false,
  timerPaused: false,
  timerReady: true,
  timerTime: mockStore.timer.time,
  unsetTimerMarker: jest.fn(),
};

beforeEach(() => {
  wrapper = shallow(<TaskClock {...taskDialProps} />);
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders task correctly', () => {
  expect(taskDialProps.createTimer).toBeCalled();
  expect(wrapper).toMatchSnapshot();
});

it('calls setCurrentTask with the correct id', () => {
  wrapper.instance().handleTimerStart();

  expect(taskDialProps.setCurrentTask).toBeCalled();
});
