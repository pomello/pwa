import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import routeComponentProps from '../../../../__mocks__/routeComponentProps';
import mockStore from '../../../../__mocks__/store';
import { getBoard, getTasks } from '../selectors';
import { TasksPage, TasksPageProps, TasksPageState } from '../TasksPage';

let wrapper: ShallowWrapper<TasksPageProps, TasksPageState, TasksPage>;

const tasksPageProps = {
  ...routeComponentProps,
  match: {
    ...routeComponentProps.match,
    params: {
      idList: 'AXRhAnTqW26gb7vb',
    },
  },
  bootstrapped: false,
  storeCurrentList: jest.fn(),
  storeListTasks: jest.fn(),
  storeTasks: jest.fn(),
};

beforeEach(() => {
  wrapper = shallow(<TasksPage {...tasksPageProps} />);
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders placeholders correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('renders tasks correctly', () => {
  wrapper.setProps({
    bootstrapped: true,
    board: getBoard(mockStore, tasksPageProps),
    tasks: getTasks(mockStore, tasksPageProps),
  });

  expect(wrapper).toMatchSnapshot();
});
