import { sortBy } from 'lodash';
import React, { SFC } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { appModels } from '../../features/app';
import AppBar from '../../features/appBar';
import MiniClock from '../../features/miniClock';
import { RootState } from '../../store/types';
import './boardsPage.scss';
import BoardsPlaceholder from './BoardsPlaceholder';

export type BoardsPageProps = BoardsPageStateProps;

interface BoardsPageStateProps {
  boards?: appModels.Boards;
  taskTimerActive: boolean;
}

const mapStateToProps = (state: RootState) => ({
  boards: state.app.boards,
  taskTimerActive: state.timer.type === 'task' && state.timer.active,
});

export const BoardsPage: SFC<BoardsPageProps> = ({
  boards,
  taskTimerActive,
}) => (
  <>
    <AppBar title="Boards" />
    <div className="list">
      {boards ? (
        <>
          {sortBy(boards, 'dateLastView')
            .reverse()
            .map(board => (
              <Link
                to={`/tasks/${board.listIds[0].shortId}`}
                key={board.id}
                className="list__item"
              >
                <span className="list__icon">
                  <span
                    className="boards__icon"
                    style={{
                      backgroundColor: board.backgroundColor,
                      backgroundImage:
                        `url(${board.backgroundImage})` || 'none',
                    }}
                  />
                </span>
                <span className="list__content">{board.name}</span>
              </Link>
            ))}
          {taskTimerActive && <MiniClock />}
        </>
      ) : (
        <BoardsPlaceholder />
      )}
    </div>
  </>
);

export default connect(mapStateToProps)(BoardsPage);
