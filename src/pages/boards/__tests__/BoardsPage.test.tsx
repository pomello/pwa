import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import mockState from '../../../../__mocks__/store';
import { BoardsPage, BoardsPageProps } from '../BoardsPage';

let wrapper: ShallowWrapper<BoardsPageProps, {}>;

beforeEach(() => {
  wrapper = shallow(<BoardsPage />);
});

it('should render without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('should render the correctly when there are no boards', () => {
  expect(wrapper).toMatchSnapshot();
});

it('should render correctly when boards are added', () => {
  wrapper.setProps({ boards: mockState.app.boards });

  expect(wrapper).toMatchSnapshot();
});
