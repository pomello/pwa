import React from 'react';
import Placeholder from '../../features/placeholder';

const BoardsPlaceholder = () => (
  <>
    {[130, 100, 160].map(size => (
      <div key={size} className="list__item">
        <span className="list__icon">
          <Placeholder size={32} type="icon" />
        </span>
        <span className="list__content">
          <Placeholder size={size} />
        </span>
      </div>
    ))}
  </>
);

export default BoardsPlaceholder;
