import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import routeComponentProps from '../../../../__mocks__/routeComponentProps';
import mockStore from '../../../../__mocks__/store';
import storage from '../../../global/helpers/storage';
import pomelloService from '../../../global/services/pomello';
import trelloService from '../../../global/services/trello';
import { BasePage, BasePageProps, BasePageState } from '../BasePage';

let wrapper: ShallowWrapper<BasePageProps, BasePageState, BasePage>;

const basePageProps = {
  ...routeComponentProps,
  appStatus: 'initializing',
  createToast: jest.fn(),
  hasBootstrapped: false,
  removePomelloUser: jest.fn(),
  removeTrelloUser: jest.fn(),
  resetSettings: jest.fn(),
  setAppStatus: jest.fn(),
  storeBoards: jest.fn(),
  storeLists: jest.fn(),
  storePomelloUser: jest.fn(),
  storeSettings: jest.fn(),
  storeTrelloUser: jest.fn(),
};

beforeEach(() => {
  wrapper = shallow(<BasePage {...basePageProps} />);
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders initializing phase correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('renders bootstrapping phase correctly', () => {
  wrapper.setProps({ appStatus: 'bootstrapping' });

  expect(basePageProps.setAppStatus).toBeCalledWith('bootstrapping');
  expect(wrapper).toMatchSnapshot();
});

it('renders boostrapped phase correctly', () => {
  wrapper.setProps({
    appStatus: 'bootstrapped',
    hasBootstrapped: true,
  });

  expect(basePageProps.setAppStatus).toHaveBeenCalledWith('bootstrapped');
  expect(wrapper).toMatchSnapshot();
});

it('does not fetch Pomello data if Pomello user already exists', () => {
  basePageProps.storePomelloUser.mockClear();
  basePageProps.storeSettings.mockClear();

  wrapper = shallow(
    <BasePage {...basePageProps} pomelloUser={mockStore.app.pomelloUser} />
  );

  expect(basePageProps.storePomelloUser).not.toHaveBeenCalled();
  expect(basePageProps.storeSettings).not.toHaveBeenCalled();
});

it('does not fetch Trello data if Trello user already exists', () => {
  basePageProps.storeTrelloUser.mockClear();
  basePageProps.storeBoards.mockClear();
  basePageProps.storeLists.mockClear();

  wrapper = shallow(
    <BasePage {...basePageProps} trelloUser={mockStore.app.trelloUser} />
  );

  expect(basePageProps.storeTrelloUser).not.toHaveBeenCalled();
  expect(basePageProps.storeBoards).not.toHaveBeenCalled();
  expect(basePageProps.storeLists).not.toHaveBeenCalled();
});

it('redirects to login page if cannot find Pomello token', () => {
  (storage.has as jest.Mock).mockImplementation(key => key !== 'pomelloToken');

  wrapper = shallow(<BasePage {...basePageProps} />);

  expect(storage.has).toHaveBeenCalledWith('pomelloToken');
  expect(basePageProps.history.replace).toBeCalledWith('/login');
});

it('redirects to connect page if cannot find Trello token', () => {
  (storage.has as jest.Mock).mockImplementation(key => key !== 'trelloToken');

  wrapper = shallow(<BasePage {...basePageProps} />);

  expect(storage.has).toHaveBeenCalledWith('trelloToken');
  expect(basePageProps.history.replace).toBeCalledWith('/connect');
});

it('shows a toast on Pomello service network error', () => {
  pomelloService.emit('error', 'network');
  expect(basePageProps.createToast).toBeCalledWith(
    'Cannot reach Pomello servers. Try again later.'
  );
});

it('shows a toast on Pomello service server error', () => {
  pomelloService.emit('error', 'server');
  expect(basePageProps.createToast).toBeCalledWith(
    'Cannot reach Pomello servers. Try again later.'
  );
});

it('removes Pomello user from storage on Pomello service auth error', () => {
  storage.set('pomelloToken', 'pomello-token');

  wrapper = shallow(<BasePage {...basePageProps} />);

  pomelloService.emit('error', 'auth');

  expect(storage.unset).toHaveBeenCalledWith('pomelloToken');
  expect(basePageProps.removePomelloUser).toHaveBeenCalled();
  expect(basePageProps.resetSettings).toHaveBeenCalled();
  expect(wrapper.state('authModalOpen')).toEqual(true);
});

it('shows a toast on Trello service network error', () => {
  trelloService.emit('error', 'network');
  expect(basePageProps.createToast).toBeCalledWith(
    'Cannot reach Trello servers. Try again later.'
  );
});

it('shows a toast on Trello service server error', () => {
  trelloService.emit('error', 'server');
  expect(basePageProps.createToast).toBeCalledWith(
    'Cannot reach Trello servers. Try again later.'
  );
});

it('removes Trello user from storage on Trello service auth error', () => {
  storage.set('trelloToken', 'trello-token');

  wrapper = shallow(<BasePage {...basePageProps} />);

  trelloService.emit('error', 'auth');

  expect(storage.unset).toHaveBeenCalledWith('trelloToken');
  expect(basePageProps.removeTrelloUser).toHaveBeenCalled();
  expect(wrapper.state('authModalOpen')).toEqual(true);
});
