import { mount, ReactWrapper } from 'enzyme';
import React from 'react';
import routeComponentProps from '../../../../__mocks__/routeComponentProps';
import storage from '../../../global/helpers/storage';
import {
  ConnectPage,
  ConnectPageProps,
  ConnectPageState,
} from '../ConnectPage';

let wrapper: ReactWrapper<ConnectPageProps, ConnectPageState, ConnectPage>;

// The wrapper for the Trello callback version
let callbackWrapper: ReactWrapper<
  ConnectPageProps,
  ConnectPageState,
  ConnectPage
>;

const connectPageProps = {
  ...routeComponentProps,
  location: {
    ...routeComponentProps.location,
    pathname: '/connect',
  },
};

beforeEach(() => {
  wrapper = mount(<ConnectPage {...connectPageProps} />);

  callbackWrapper = mount(
    <ConnectPage
      {...connectPageProps}
      location={{
        ...connectPageProps.location,
        hash:
          '#token=1234567890123456789012345678901234567890123456789012345678901234',
      }}
    />
  );
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders non-callback version correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('renders callback version correctly', () => {
  expect(callbackWrapper.state('isTrelloCallback')).toEqual(true);
  expect(callbackWrapper).toMatchSnapshot();
});

it('calls storage.set in callback version', () => {
  expect(storage.set).toHaveBeenCalledTimes(1);
  expect(storage.set).toHaveBeenCalledWith(
    'trelloToken',
    '1234567890123456789012345678901234567890123456789012345678901234'
  );
});
