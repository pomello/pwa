import { mount, ReactWrapper } from 'enzyme';
import React from 'react';
import routeComponentProps from '../../../../__mocks__/routeComponentProps';
import storage from '../../../global/helpers/storage';
import pomelloService from '../../../global/services/pomello';
import { LoginPage, LoginPageProps, LoginPageState } from '../LoginPage';

let wrapper: ReactWrapper<LoginPageProps, LoginPageState, LoginPage>;

// The wrapper for the Google callback version
let callbackWrapper: ReactWrapper<LoginPageProps, LoginPageState, LoginPage>;

const loginPageProps = {
  ...routeComponentProps,
  location: {
    ...routeComponentProps.location,
    pathname: '/login',
  },
};

beforeEach(() => {
  wrapper = mount(<LoginPage {...loginPageProps} />);

  callbackWrapper = mount(
    <LoginPage
      {...loginPageProps}
      location={{
        ...loginPageProps.location,
        search: '?code=123456',
      }}
    />
  );
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders non-callback version correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('renders callback version correctly', () => {
  expect(callbackWrapper.state('isGoogleCallback')).toEqual(true);
  expect(callbackWrapper).toMatchSnapshot();
});

it('renders callback error correctly', () => {
  callbackWrapper.setState({ hasGoogleCallbackError: true });

  expect(callbackWrapper).toMatchSnapshot();
});

it('calls storage.set when authenticated with Google', async () => {
  expect(pomelloService.authGoogleUser).toHaveBeenCalledWith('?code=123456');
  expect(storage.set).toHaveBeenCalled();
  expect(storage.set).toHaveBeenCalledWith('pomelloToken', 'pomello-token');
});
