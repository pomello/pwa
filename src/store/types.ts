import { StateType } from 'typesafe-actions';
import { AppAction } from '../features/app';
import { OvertimeAction } from '../features/overtime';
import { TimerAction } from '../features/timer';
import { ToastAction } from '../features/toast';
import rootReducer from './reducers';

export type RootState = StateType<typeof rootReducer>;

export type RootAction = AppAction & OvertimeAction & TimerAction & ToastAction;
