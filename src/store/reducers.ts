import { combineReducers } from 'redux';
import { appReducer as app } from '../features/app';
import { overtimeReducer as overtime } from '../features/overtime';
import { timerReducer as timer } from '../features/timer';
import { toastReducer as toast } from '../features/toast';

const rootReducer = combineReducers({ app, overtime, timer, toast });

export default rootReducer;
