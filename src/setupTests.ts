import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-enzyme';
import '../__mocks__/pomelloService';
import '../__mocks__/storage';
import '../__mocks__/trelloService';

configure({ adapter: new Adapter() });

afterEach(() => {
  jest.clearAllMocks();
});
