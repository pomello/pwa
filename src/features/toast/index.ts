import { ActionType } from 'typesafe-actions';
import { Omit } from 'utility-types';
import * as toastActions from './actions';
import * as toastModels from './models';
import toastReducer from './reducer';

type ToastsStandardActions = Omit<typeof toastActions, 'createToast'>;

export type ToastAction = ActionType<ToastsStandardActions>;
export { default } from './components/Toast';
export { toastActions, toastModels, toastReducer };
