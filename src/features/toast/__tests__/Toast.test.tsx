import { mount, ReactWrapper } from 'enzyme';
import React from 'react';
import { Toasts, ToastsProps, ToastState } from '../components/Toast';

let wrapper: ReactWrapper<ToastsProps, ToastState, Toasts>;

beforeEach(() => {
  wrapper = mount(<Toasts toast={null} />);
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders a toast correctly', () => {
  wrapper.setProps({ toast: 'Foobar' });

  expect(wrapper).toMatchSnapshot();
});
