import { ToastAction, toastActions } from '..';
import mockStore from '../../../../__mocks__/store';
import reducer, { toastDefaultState } from '../reducer';

it('should return the initial state', () => {
  expect(reducer(undefined, {} as ToastAction)).toEqual(toastDefaultState);
});

it('should handle endQueue', () => {
  expect(
    reducer(toastDefaultState, toastActions.endQueue()).isProcessingQueue
  ).toEqual(false);
});

it('should handle processToastQueue', () => {
  expect(
    reducer(
      {
        ...toastDefaultState,
        queue: mockStore.toast.queue,
      },
      toastActions.processToastQueue()
    )
  ).toEqual({
    ...toastDefaultState,
    isProcessingQueue: true,
    current: mockStore.toast.current,
    queue: ['Toast 2', 'Toast 3'],
  });
});

it('should handle queueToast', () => {
  expect(
    reducer(
      {
        ...toastDefaultState,
        queue: ['Toast 1', 'Toast 2'],
      },
      toastActions.queueToast('Toast 3')
    ).queue
  ).toEqual(mockStore.toast.queue);
});

it('should handle removeToast', () => {
  expect(
    reducer(
      {
        ...toastDefaultState,
        current: mockStore.toast.current,
      },
      toastActions.removeToast()
    ).current
  ).toBeNull();
});
