import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import LoadingButton, {
  LoadingButtonDefaultProps,
  LoadingButtonProps,
  LoadingButtonState,
} from '../components/LoadingButton';

let wrapper: ShallowWrapper<
  LoadingButtonProps & LoadingButtonDefaultProps,
  LoadingButtonState,
  LoadingButton
>;

beforeEach(() => {
  wrapper = shallow(<LoadingButton loading={false} />);
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders correctly with default values', () => {
  expect(wrapper).toMatchSnapshot();
});

it('renders correctly with different button types', () => {
  wrapper.setProps({ type: 'reset' });

  expect(wrapper).toMatchSnapshot();
});

it('renders correctly with different button values', () => {
  wrapper.setProps({ value: 'Foobar' });

  expect(wrapper).toMatchSnapshot();
});

it('renders correctly when loading', () => {
  wrapper.setProps({ loading: true });

  expect(wrapper).toMatchSnapshot();
});
