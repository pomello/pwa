export type Marker = {
  time: Time;
  timestamp: number;
};

export type Markers = {
  [key: string]: Marker;
};

export type Settings = {
  endSoundSrc: string | false;
  endSoundVol: number | false;
  startSoundSrc: string | false;
  startSoundVol: number | false;
  startTime: number;
  tickSoundSrc: string | false;
  tickSoundVol: number | false;
  type: string;
};

export type Time = number;
