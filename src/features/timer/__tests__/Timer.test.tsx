import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { Timer, TimerProps } from '../components/Timer';

let wrapper: ShallowWrapper<TimerProps, {}, Timer>;

const mockDecrementTime = jest.fn();
const mockDestroyTimer = jest.fn();
const mockStartTimer = jest.fn();

beforeEach(() => {
  jest.useFakeTimers();
  jest.clearAllMocks();

  wrapper = shallow(
    <Timer
      active={false}
      decrementTime={mockDecrementTime}
      destroyTimer={mockDestroyTimer}
      endSoundSrc={null}
      endSoundVol={0}
      paused={false}
      startSoundSrc={null}
      startSoundVol={0}
      startTimer={mockStartTimer}
      tickSoundSrc={null}
      tickSoundVol={0}
    />
  );
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('calls decrementTime when timer is active', () => {
  wrapper.setProps({ active: true, time: 5 });

  jest.advanceTimersByTime(5000);

  expect(setTimeout).toHaveBeenCalled();
  expect(mockDecrementTime).toHaveBeenCalled();
});

it('calls destroyTimer when time runs out', () => {
  wrapper.setProps({ active: true, time: 1 });
  wrapper.setProps({ time: 0 });

  expect(mockDestroyTimer).toHaveBeenCalled();
});
