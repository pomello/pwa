import { TimerAction, timerActions } from '..';
import reducer, { timerDefaultState } from '../reducer';
import mockStore from '../../../../__mocks__/store';

it('should render the initial state', () => {
  expect(reducer(undefined, {} as TimerAction)).toEqual(timerDefaultState);
});

it('should handle createTimer', () => {
  expect(
    reducer(
      timerDefaultState,
      timerActions.createTimer({
        endSoundSrc: 'ding',
        endSoundVol: 1,
        startSoundSrc: 'wind-up',
        startSoundVol: 1,
        startTime: 1500,
        tickSoundSrc: 'egg-timer',
        tickSoundVol: 1,
        type: 'task',
      })
    )
  ).toEqual({ ...mockStore.timer, active: false });
});

it('should handle decrementTime', () => {
  expect(reducer(mockStore.timer, timerActions.decrementTime())).toEqual({
    ...mockStore.timer,
    humanTime: '24:59',
    time: 1499,
  });
});

it('should handle destroyTimer', () => {
  expect(reducer(mockStore.timer, timerActions.destroyTimer())).toEqual(
    timerDefaultState
  );
});

it('should handle pauseTimer', () => {
  expect(reducer(mockStore.timer, timerActions.pauseTimer())).toEqual({
    ...mockStore.timer,
    paused: true,
  });
});

it('should handle resumeTimer', () => {
  expect(
    reducer(
      {
        ...mockStore.timer,
        paused: true,
      },
      timerActions.resumeTimer()
    )
  ).toEqual(mockStore.timer);
});

it('should handle setMarker', () => {
  expect(
    reducer(
      mockStore.timer,
      timerActions.setMarker({
        id: 'testMarker',
        timestamp: 100,
      })
    ).markers.testMarker
  ).toEqual({
    time: 1500,
    timestamp: 100,
  });
});

it('should handle startTimer', () => {
  expect(
    reducer(
      {
        ...mockStore.timer,
        active: false,
      },
      timerActions.startTimer()
    )
  ).toEqual(mockStore.timer);
});

it('should handle unsetMarker', () => {
  expect(
    reducer(
      {
        ...mockStore.timer,
        markers: {
          testMarker: {
            time: 1500,
            timestamp: 100,
          },
        },
      },
      timerActions.unsetMarker('testMarker')
    ).markers
  ).toEqual({});
});
