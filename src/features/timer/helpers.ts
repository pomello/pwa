export const humanTime = (time: number) => {
  const minutes = Math.floor(time / 60);
  const seconds = time % 60;

  const minuteString = minutes < 10 ? `0${minutes}` : `${minutes}`;
  const secondString = seconds < 10 ? `0${seconds}` : `${seconds}`;

  return `${minuteString}:${secondString}`;
};
