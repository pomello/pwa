import { ActionType } from 'typesafe-actions';
import { Omit } from 'utility-types';
import * as timerActions from './actions';
import * as timerModels from './models';
import timerReducer from './reducer';

type TimerStandardActions = Omit<typeof timerActions, 'addMarker'>;

export type TimerAction = ActionType<TimerStandardActions>;
export { default } from './components/Timer';
export { timerActions, timerModels, timerReducer };
