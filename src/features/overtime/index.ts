import { ActionType } from 'typesafe-actions';
import { Omit } from 'utility-types';
import * as overtimeActions from './actions';
import * as overtimeModels from './models';
import overtimeReducer from './reducer';

type OvertimeStandardActions = Omit<typeof overtimeActions, 'startOvertime'>;

export type OvertimeAction = ActionType<OvertimeStandardActions>;
export { default } from './components/Overtime';
export { overtimeActions, overtimeModels, overtimeReducer };
