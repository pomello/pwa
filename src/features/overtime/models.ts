export type Active = boolean;

export type EventId = string | null;

export type HumanTime = string;

export type Time = number;

export type Timestamp = number;

export type Type = string;
