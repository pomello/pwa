import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import Placeholder, { PlaceholderProps } from '../components/Placeholder';

let wrapper: ShallowWrapper<PlaceholderProps, {}>;

beforeEach(() => {
  wrapper = shallow(<Placeholder size={100} />);
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('renders correctly with specific length', () => {
  wrapper.setProps({ size: 120 });

  expect(wrapper).toMatchSnapshot();
});

it('renders icon placeholder correctly', () => {
  wrapper.setProps({ type: 'icon' });

  expect(wrapper).toMatchSnapshot();
});
