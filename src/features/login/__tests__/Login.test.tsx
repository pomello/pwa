import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import storage from '../../../global/helpers/storage';
import Login, { LoginState } from '../components/Login';

let wrapper: ShallowWrapper<{}, LoginState, Login>;

beforeEach(() => {
  wrapper = shallow(<Login />);
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('renders a error message with bad credentials', async () => {
  wrapper.setState({ passwordInput: 'bad password' });

  await wrapper.instance().authenticateUser();

  expect(wrapper).toMatchSnapshot();
});

it('updates localStorage when authenticated', async () => {
  await wrapper.instance().authenticateUser();

  expect(storage.set).toHaveBeenCalled();
  expect(storage.set).toHaveBeenCalledWith('pomelloToken', 'pomello-token');
});
