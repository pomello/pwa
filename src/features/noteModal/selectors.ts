import { createSelector } from 'reselect';
import { RootState } from '../../store/types';

const getCurrentTask = (state: RootState) => state.app.currentTask;

const getTasks = (state: RootState) => state.app.tasks;

export const getTask = createSelector(
  getTasks,
  getCurrentTask,
  (tasks, currentTask) => {
    if (tasks && currentTask) {
      return tasks[currentTask];
    }
  }
);
