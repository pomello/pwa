import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { Clock, ClockProps, ClockState } from '../components/Clock';

let wrapper: ShallowWrapper<ClockProps, ClockState, Clock>;

const mockStartTimer = jest.fn();

beforeEach(() => {
  wrapper = shallow(
    <Clock
      actions={[
        {
          id: 'item-1',
          icon: 'item-1-icon.svg',
          label: 'Item 1',
          callback: () => null,
        },
        {
          id: 'item-2',
          icon: 'item-2-icon.svg',
          label: 'Item 2',
          callback: () => null,
        },
      ]}
      autostart={true}
      eyebrow="Eyebrow"
      heading="Heading"
      loading={false}
      startTimer={mockStartTimer}
      timerActive={false}
      timerReady={true}
    />
  );
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('renders placeholders correctly', () => {
  wrapper.setProps({ loading: true, timerReady: false });

  expect(wrapper).toMatchSnapshot();
});

it('renders extra actions correctly', () => {
  wrapper.setState({ actionsSheetOpen: true });

  expect(wrapper).toMatchSnapshot();
});

it('calls startTimer if autoStart is enabled', () => {
  expect(mockStartTimer).toHaveBeenCalledTimes(1);
});
