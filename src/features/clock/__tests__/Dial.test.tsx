import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { Dial, DialProps } from '../../clock/components/Dial';

let wrapper: ShallowWrapper<DialProps, {}, Dial>;

const mockActionsCallback = jest.fn();
const mockPauseTimer = jest.fn();
const mockResumeTimer = jest.fn();
const mockStartCallback = jest.fn();
const mockStartTimer = jest.fn();

beforeEach(() => {
  wrapper = shallow(
    <Dial
      actionsCallback={mockActionsCallback}
      pauseTimer={mockPauseTimer}
      resumeTimer={mockResumeTimer}
      startCallback={mockStartCallback}
      startTimer={mockStartTimer}
      time="25:00"
      timerActive={false}
      timerPaused={false}
    />
  );
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('renders correctly when timer is loading', () => {
  wrapper.setProps({ time: undefined });

  expect(wrapper).toMatchSnapshot();
});

it('calls startTimer when dial is clicked', () => {
  wrapper.find('button.dial__container').simulate('click');

  expect(mockStartTimer).toBeCalled();
  expect(mockStartCallback).toBeCalled();
});

it('calls startTimer when "Press to start" is clicked', () => {
  wrapper.find('button.dial__start').simulate('click');

  expect(mockStartTimer).toBeCalled();
  expect(mockStartCallback).toBeCalled();
});

it('calls pauseTimer when dial is clicked and timer is active', () => {
  wrapper.setProps({ timerActive: true });
  wrapper.find('button.dial__container').simulate('click');

  expect(mockPauseTimer).toBeCalled();
});

it('calls resumeTimer when dial is clicked and timer is paused', () => {
  wrapper.setProps({ timerActive: true, timerPaused: true });
  wrapper.find('button.dial__container').simulate('click');

  expect(mockResumeTimer).toBeCalled();
});

it('calls actionsCallback when actions button is clicked and timer is active', () => {
  wrapper.setProps({ timerActive: true });
  wrapper.find('button.dial__actions').simulate('click');

  expect(mockActionsCallback).toBeCalled();
});
