import { mount, ReactWrapper } from 'enzyme';
import React from 'react';
import Sheet, { SheetProps } from '../components/Sheet';

let wrapper: ReactWrapper<SheetProps, {}>;

const handleClose = jest.fn();
const itemCallback = jest.fn();

beforeEach(() => {
  wrapper = mount(
    <Sheet
      handleClose={handleClose}
      isOpen={true}
      items={[
        {
          id: 'item-1',
          label: 'Item 1',
          callback: itemCallback,
        },
        {
          id: 'item-2',
          icon: 'item-2-src',
          label: 'Item 2',
          callback: () => true,
        },
      ]}
    />
  );
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('renders icons correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('calls itemCallback and handleClose when item is clicked', () => {
  wrapper
    .find('.sheet__item')
    .at(0)
    .simulate('click');

  expect(itemCallback).toBeCalled();
  expect(handleClose).toBeCalled();
});

it('calls handleClose when the overlay is clicked', () => {
  wrapper.find('.sheet__overlay').simulate('click');

  expect(handleClose).toBeCalled();
});
