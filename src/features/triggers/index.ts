import EventEmitter from 'events';

const triggersEmitter = new EventEmitter();

export { default } from './components/Triggers';
export { triggersEmitter };
