import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { AppBar, AppBarProps } from '../components/AppBar';

let wrapper: ShallowWrapper<AppBarProps>;

const mockShowMenu = jest.fn();

beforeEach(() => {
  wrapper = shallow(<AppBar showMenu={mockShowMenu} />);
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders a title correctly', () => {
  wrapper.setProps({ title: 'Test title ' });

  expect(wrapper).toMatchSnapshot();
});

it('renders transparent version correctly', () => {
  wrapper.setProps({ transparent: true });

  expect(wrapper).toMatchSnapshot();
});

it('renders a return route correctly', () => {
  wrapper.setProps({ returnRoute: '/boards' });

  expect(wrapper).toMatchSnapshot();
});
