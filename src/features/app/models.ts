import { Overwrite } from 'utility-types';
import { timerModels } from '../timer';

export type GracePeriod = {
  time: timerModels.Time;
  timestamp: number;
};

export type Board = {
  backgroundColor: string;
  backgroundImage: string | null;
  dateLastView: number;
  id: string;
  listIds: Array<{
    id: string;
    shortId: string;
  }>;
  name: string;
  shortId: string;
};

export type Boards = {
  [key: string]: Board;
};

export type Bootstrapped = boolean;

export type CurrentList = List['shortId'] | null;

export type CurrentTask = Task['id'] | null;

export type List = {
  id: string;
  idBoard: string;
  name: string;
  shortId: string;
  shortIdBoard: string;
  tasks?: ListTasks;
};

export type Lists = {
  [key: string]: List;
};

export type ListTasks = string[];

export type ListTasksPayload = {
  idList: string;
  listTasks: ListTasks;
};

export type MenuOpen = boolean;

export type NextBreak = string;

export type PomelloTask = {
  id: string;
  serviceId: string;
  parentId: string | null;
  pomodoros: number;
  voidedPomodoros: number;
  shortBreaks: number;
  longBreaks: number;
  internalDistractions: number;
  externalDistractions: number;
  notes: number;
  taskTime: number;
  pauseTime: number;
  overTaskTime: number;
  breakTime: number;
  over_breakTime: number;
  voidTime: number;
};

export type PomelloUser = {
  name: string;
  email: string;
  timezone: string;
  type: string;
};

export type PomelloEvent = {
  children: [];
  id: string;
  meta: { duration: number; allottedTime: number; pomodoros: number };
  parentId: string | null;
  parentServiceId: string | null;
  startTime: string;
  type: string;
};

export type Pomodoros = number;

export type Settings = {
  alwaysOnTop: boolean;
  autoStartBreaks: boolean;
  autoStartTasks: boolean;
  betweenTasksGracePeriod: number;
  checkPomelloStatus: boolean;
  completedTaskPosition: string;
  createdTaskPosition: string;
  longBreakTime: number;
  longBreakTimerEndSound: string;
  longBreakTimerEndVol: number;
  longBreakTimerStartSound: string;
  longBreakTimerStartVol: number;
  longBreakTimerTickSound: string;
  longBreakTimerTickVol: number;
  overtime: boolean;
  overtimeDelay: number;
  pomodoroSet: number;
  productivityChartDays: string[];
  productivityChartType: string;
  pwaPreventDisplaySleep: boolean;
  resetPomodoroSet: boolean;
  selectMaxRows: number;
  shortBreakTime: number;
  shortBreakTimerEndSound: string;
  shortBreakTimerEndVol: number;
  shortBreakTimerStartSound: string;
  shortBreakTimerStartVol: number;
  shortBreakTimerTickSound: string;
  shortBreakTimerTickVol: number;
  showAddNoteButton: boolean;
  showMenuButton: boolean;
  showPauseButton: boolean;
  showSwitchTaskButton: boolean;
  showTaskFinishedButton: boolean;
  showVoidTaskButton: boolean;
  snapEdges: boolean;
  taskTime: number;
  taskTimerEndSound: string;
  taskTimerEndVol: number;
  taskTimerStartSound: string;
  taskTimerStartVol: number;
  taskTimerTickSound: string;
  taskTimerTickVol: number;
  timeExpiredNotification: string;
  timestamp: number | null;
  titleFormat: string;
  titleMarker: string;
  warnBeforeAppQuit: boolean;
  warnBeforeTaskCancel: boolean;
};

export type Status = string;

export type Task = TaskCard | TaskCheckItem;

export type TaskCard = {
  checklists: TaskCardChecklist[];
  id: string;
  idBoard: string;
  idCard: undefined;
  idList: string;
  name: string;
  shortId: string;
  shortIdBoard: string;
  shortIdCard: undefined;
  shortIdList: string;
};

export type TaskCardChecklist = {
  checkItems: string[];
  id: string;
  name: string;
};

export type TaskCheckItem = Overwrite<
  TaskCard,
  {
    checklists: undefined;
    idCard: string;
    shortIdCard: string;
  }
>;

export type Tasks = {
  [key: string]: Task;
};

export type TrelloUser = {
  id: string;
};
