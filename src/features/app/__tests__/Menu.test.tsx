import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import Menu, { MenuProps } from '../components/Menu';

let wrapper: ShallowWrapper<MenuProps, Menu>;

const mockHandleMenuHide = jest.fn();

beforeEach(() => {
  wrapper = shallow(
    <Menu
      handleMenuHide={mockHandleMenuHide}
      bootstrapped={true}
      transitionState="entered"
    />
  );
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('hides the menu when overlay is clicked', () => {
  wrapper.find('.menu__overlay').simulate('click');

  expect(mockHandleMenuHide).toHaveBeenCalled();
});
