import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import routeComponentProps from '../../../../__mocks__/routeComponentProps';
import { App, AppProps } from '../components/App';

let wrapper: ShallowWrapper<AppProps>;

const appRouteProps = {
  bootstrapped: true,
  hideMenu: jest.fn(),
  incrementPomodoros: jest.fn(),
  location: routeComponentProps.location,
  menuOpen: false,
  timerActive: true,
  timerType: 'task',
};

beforeEach(() => {
  wrapper = shallow(<App {...appRouteProps} />);
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('calls incrementPomodoros when a task timer ends', () => {
  wrapper.setProps({ timerActive: false });

  expect(appRouteProps.incrementPomodoros).toBeCalled();
});
