import { omit } from 'lodash';
import { AppAction, appActions } from '..';
import mockStore from '../../../../__mocks__/store';
import pomelloService from '../../../global/services/pomello';
import trelloService from '../../../global/services/trello';
import reducer, { appDefaultState } from '../reducer';

it('should return the initial state', () => {
  expect(reducer(undefined, {} as AppAction)).toEqual(appDefaultState);
});

it('should handle hideMenu', () => {
  expect(reducer(appDefaultState, appActions.hideMenu()).menuOpen).toEqual(
    false
  );
});

it('should handle incrementPomodoros', () => {
  const firstPomodoro = reducer(
    appDefaultState,
    appActions.incrementPomodoros()
  );

  const secondPomodoro = reducer(
    firstPomodoro,
    appActions.incrementPomodoros()
  );

  const fourthPomodoro = reducer(
    {
      ...appDefaultState,
      pomodoros: 3,
      prevPomodoros: 2,
    },
    appActions.incrementPomodoros()
  );

  expect(firstPomodoro.pomodoros).toEqual(1);
  expect(firstPomodoro.prevPomodoros).toEqual(0);
  expect(firstPomodoro.nextBreak).toEqual('short');

  expect(secondPomodoro.pomodoros).toEqual(2);
  expect(secondPomodoro.prevPomodoros).toEqual(1);
  expect(secondPomodoro.nextBreak).toEqual('short');

  expect(fourthPomodoro.pomodoros).toEqual(0);
  expect(fourthPomodoro.prevPomodoros).toEqual(3);
  expect(fourthPomodoro.nextBreak).toEqual('long');
});

it('should handle removePomelloUser', () => {
  expect(
    reducer(
      {
        ...appDefaultState,
        pomelloUser: mockStore.app.pomelloUser,
      },
      appActions.removePomelloUser()
    ).pomelloUser
  ).toBeUndefined();
});

it('should handle removeTrelloUser', () => {
  expect(
    reducer(
      {
        ...appDefaultState,
        trelloUser: mockStore.app.trelloUser,
      },
      appActions.removeTrelloUser()
    ).trelloUser
  ).toBeUndefined();
});

it('should handle resetPomodors', () => {
  expect(
    reducer({ ...appDefaultState, pomodoros: 4 }, appActions.resetPomodoros())
      .pomodoros
  ).toEqual(0);
});

it('should handle resetSettings', () => {
  expect(
    reducer(
      {
        ...appDefaultState,
        settings: mockStore.app.settings,
      },
      appActions.resetSettings()
    ).settings
  ).toEqual(appDefaultState.settings);
});

it('should handle setCurrentList', () => {
  const listId = mockStore.app.lists.AXRhAnTqW26gb7vb.shortId;

  expect(
    reducer(
      {
        ...appDefaultState,
        lists: mockStore.app.lists,
      },
      appActions.setCurrentList(listId)
    ).currentList
  ).toEqual(mockStore.app.currentList);
});

it('should handle setCurrentTask', () => {
  const taskId = mockStore.app.tasks.B1ZBLgSv7yLmH9u9.shortId;

  const startTime = {
    time: mockStore.timer.time,
    timestamp: 100,
  };

  const state = reducer(
    appDefaultState,
    appActions.setCurrentTask({
      id: taskId,
      startTime,
    })
  );

  expect(state.currentTask).toEqual(taskId);
  expect(state.currentTaskStartTime).toEqual(startTime);
});

it('should handle setStatus', () => {
  expect(
    reducer(appDefaultState, appActions.setStatus('bootstrapped'))
  ).toHaveProperty('status', 'bootstrapped');
});

it('should handle showMenu', () => {
  expect(
    reducer({ ...appDefaultState, menuOpen: true }, appActions.showMenu())
      .menuOpen
  ).toEqual(true);
});

it('should handle storeBoards', async () => {
  const response = await trelloService.fetchBoardsAndLists();

  if (response.success) {
    const action = appActions.storeBoards(response.data.boards);
    expect(reducer(appDefaultState, action).boards).toEqual(
      mockStore.app.boards
    );
  }
});

it('should handle storeListTasks', async () => {
  const idList = '5bd70c16706ddd7f5ed22511';
  const state = {
    ...appDefaultState,
    lists: omit(mockStore.app.lists, `${idList}.tasks`),
  };
  const response = await trelloService.fetchCardsAndChecklists(idList);

  if (response.success) {
    const action = appActions.storeListTasks({
      idList: idList,
      listTasks: response.data.listsTasks,
    });

    expect(reducer(state, action).lists).toEqual(mockStore.app.lists);
  }
});

it('should handle storeLists', async () => {
  const response = await trelloService.fetchBoardsAndLists();

  if (response.success) {
    const action = appActions.storeLists(response.data.lists);
    const listsWithoutTasks = omit(
      mockStore.app.lists,
      'AXRhAnTqW26gb7vb.tasks'
    );

    expect(reducer(appDefaultState, action).lists).toEqual(listsWithoutTasks);
  }
});

it('should handle storePomelloUser', async () => {
  const response = await pomelloService.fetchUser();

  if (response.success) {
    const action = appActions.storePomelloUser(response.data);
    expect(reducer(appDefaultState, action).pomelloUser).toEqual(
      mockStore.app.pomelloUser
    );
  }
});

it('should handle storeSettings', async () => {
  const response = await pomelloService.fetchSettings();

  if (response.success) {
    const action = appActions.storeSettings(response.data);
    expect(reducer(appDefaultState, action).settings).toEqual(
      mockStore.app.settings
    );
  }
});

it('should handle storeTasks', async () => {
  const response = await trelloService.fetchCardsAndChecklists(
    '5bd70c16706ddd7f5ed22511'
  );

  if (response.success) {
    const action = appActions.storeTasks(response.data.tasks);
    expect(reducer(appDefaultState, action).tasks).toEqual(mockStore.app.tasks);
  }
});

it('should handle storeTrelloUser', async () => {
  const response = await trelloService.fetchUser();

  if (response.success) {
    const action = appActions.storeTrelloUser(response.data);
    expect(reducer(appDefaultState, action).trelloUser).toEqual(
      mockStore.app.trelloUser
    );
  }
});

it('should handle unsetCurrentList', () => {
  expect(
    reducer(
      {
        ...appDefaultState,
        currentList: mockStore.app.currentList,
      },
      appActions.unsetCurrentList()
    ).currentList
  ).toBeNull();
});

it('should handle unsetCurrentTask', () => {
  const taskId = mockStore.app.tasks.B1ZBLgSv7yLmH9u9.id;

  expect(
    reducer(
      { ...appDefaultState, currentTask: taskId },
      appActions.unsetCurrentTask()
    ).currentTask
  ).toBeNull();
});
