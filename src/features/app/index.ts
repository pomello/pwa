import { ActionType } from 'typesafe-actions';
import { Omit } from 'utility-types';
import * as appActions from './actions';
import * as appModels from './models';
import appReducer from './reducer';

type AppStandardActions = Omit<typeof appActions, 'startGracePeriod'>;

export type AppAction = ActionType<AppStandardActions>;
export { default } from './components/App';
export { appActions, appModels, appReducer };
