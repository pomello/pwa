import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import Modal, { ModalProps } from '../components/Modal';

let wrapper: ShallowWrapper<ModalProps, {}, Modal>;

const handleClose = jest.fn();
const buttonCallback = jest.fn();

beforeEach(() => {
  wrapper = shallow(
    <Modal
      handleClose={handleClose}
      buttons={[
        { label: 'Button 1', callback: buttonCallback },
        { label: 'Button 2' },
      ]}
    >
      Hello world
    </Modal>
  );
});

it('renders without crashing', () => {
  expect(wrapper).toHaveLength(1);
});

it('renders correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

it('calls handleClose when button is clicked', () => {
  wrapper
    .find('button')
    .at(0)
    .simulate('click');
  wrapper
    .find('button')
    .at(1)
    .simulate('click');

  expect(handleClose).toHaveBeenCalledTimes(2);
});

it('calls buttonCallback when button 1 is clicked', () => {
  wrapper
    .find('button')
    .at(0)
    .simulate('click');

  expect(buttonCallback).toHaveBeenCalled();
});
