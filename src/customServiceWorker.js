workbox.routing.registerRoute(
  new RegExp(
    [
      'https://api.trello.com.*',
      'https://trello-backgrounds.s3.amazonaws.com.*',
      '<%= REACT_APP_POMELLO_URL %>.*',
    ].join('|')
  ),
  workbox.strategies.networkFirst()
);

self.addEventListener('message', (event) => {
  if (!event.data) {
    return;
  }

  switch (event.data) {
    case 'skipWaiting':
      self.skipWaiting();
      break;
    default:
      break;
  }
});
