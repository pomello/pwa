import { PomelloService } from '..';
import { appModels } from '../../../../features/app';

type LogEventSuccess = {
  success: true;
  data: appModels.PomelloEvent;
};

type LogEventFailure = {
  success: false;
};

export default async function(
  this: PomelloService,
  data: any
): Promise<LogEventSuccess | LogEventFailure> {
  try {
    const response = await this.api.post('/events', data);

    this.storeEvent(response.data);

    return {
      success: true,
      data: response.data,
    };
  } catch (error) {
    return { success: false };
  }
}
