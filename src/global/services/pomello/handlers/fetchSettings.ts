import { PomelloService } from '..';
import { appModels } from '../../../../features/app';

type FetchSettingsSuccess = {
  success: true;
  data: appModels.Settings;
};

type FetchSettingsFailure = {
  success: false;
};

export default async function(
  this: PomelloService
): Promise<FetchSettingsSuccess | FetchSettingsFailure> {
  try {
    const response = await this.api.get('/settings');

    return {
      success: true,
      data: response.data,
    };
  } catch (error) {
    return { success: false };
  }
}
