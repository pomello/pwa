const getSoundSrc = (sound: string) => {
  if (sound === 'ding' || sound === 'egg-timer' || sound === 'wind-up') {
    return `/sounds/${sound}.ogg`;
  }

  return sound;
};

export default getSoundSrc;
