export default function() {
  return Math.ceil(new Date().getTime() / 1000);
}
