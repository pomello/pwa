import { RootState } from '../../store/types';

export const getTask = ({ app: { currentTask, tasks } }: RootState) =>
  currentTask && tasks ? tasks[currentTask] : null;
