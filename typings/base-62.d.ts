declare module 'base-62.js' {
  export function encode(hex: string): string;
  export function decode(hex: string): string;
  export function encodeHex(hex: string): string;
  export function decodeHex(hex: string): string;
  export function random(): string;
}