declare module 'nosleep.js' {
  class NoSleep {
    constructor();
    enable(): void;
    disable(): void;
  }

  export = NoSleep;
}
