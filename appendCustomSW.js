const { template } = require('lodash');
const dotenv = require('dotenv');
const fs = require('fs');
const path = require('path');

dotenv.config({ path: path.resolve(process.cwd(), '.env.local') });

const customServiceWorker = fs.readFileSync('src/customServiceWorker.js');

const parsedCustomServiceWorker = template(customServiceWorker)(process.env);

fs.appendFileSync('build/service-worker.js', parsedCustomServiceWorker);
